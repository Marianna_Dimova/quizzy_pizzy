# Quizzy pizzy

<br/>

## Description

**Final Project Assignment for "Telerik Academy Alpha" with JavaScript:** </br>
Design and implement a single-page web application that will allow students to solve quizzes. Teachers can manage quizzes, students can solve them and check the leaderboard.

### Students can:

-authenticate users - register/login
-Solve quizzes

### have:

-Leaderboard
-History page of solved quizzes
-Profile page

### Teachers can:

- Create and solve quizzes

### have:

- History page of solved quizzes
- Profile page
  <br/>

---

## Project Requirements

<br/>

### Server

1. To run our project locally, clone this repository.

   Run `npm install` in the main folder directory.

   Please use latest version of `node`.

   To run the application locally and visit `[localhost:3000]` to explore.

2. Setup MySQL Database with new Schema.

3. Setup `config.js` file. It needs to be on root level in api folder where is `package.json` and other config files.

   - `config.json` file with your settings:

   ```sh
    {
      host: 'localhost',
      port: '3306',
      user: 'root',
      password: 'your password',
      database: 'my_db',
     };
   ```

4. After files are setup, open the terminal and run the following command:

   `npm run start:dev`

---

### Endpoints

For your convenience, we provide a brief description of the endpoints and some previews

### For Category

**GET '/categories/'** = Will retrieve all categories for quizzes

**POST '/categories/'** = Create category by a teacher

```js
{
  "category_name": string
}
```

### For Quizzes

**GET 'categories/:id/quizzes'** = Will retrieve all quizzes in category

**Post 'categories/:id/'** = Create quiz by a teacher

```js
{
  "quiz_name": string,
  "required_time": number,
}
```

**PUT 'categories/:categoryId/quizzes/:quizId/status'** = Activate created quiz

**POST 'categories/:categoryId/quizzes/:quizId/answer'** = Answers creation for current quiz

**GET 'categories/quizzes/history'** = Get information from quiz history about solved quizzes

**POST 'questions/:id/create'** = Create questions for current quiz

**GET 'questions/start/quiz/:quizId'** = Get quiz with questions and available answers

**POST '/questions/quiz/:quizId/submit'** = Submit resolved quiz

**GET '/questions/leader_board'** = Get leaderboard information

### _For Users_

**POST '/users/'** = Will register a new user

Request Body

```js
{
    "username": string,
    "password": string,
    "firstName": string,
    "lastName": string,
    "gender": string,
}
```

**POST '/auth/login'** = Will login an existing user

Request Body

```js
{
    "username": string,
    "password": string,
}
```

---

### Client

5. Navigate to the `client` folder. Open the terminal and run the following commands:

   `npm install`

   `npm run start`

### Built With

- [React JS](https://reactjs.org/)
- [Express.js](https://expressjs.com/)
- [MariaDB](https://mariadb.org/)
- [Reactstrap](https://reactstrap.github.io/)
- [JWT](https://jwt.io/)

---

### What helped in our research and coding:

- [React Documentation](https://reactjs.org/docs/getting-started.html)
- [Express.js Documentation](https://expressjs.com/)
- StackOverflow, Medium, etc.

---

### Special thanks goes to:

Our devoted trainers - _Edward Evlogiev_ and _Rosen Urkov_
and our mentor - _Nikolay Neykov_ <3

---

### Authors and Contributors

- Daniel Dimitrov - `d.dimitrov9610@gmail.com`
- Marianna Dimova - `m1marianna1m1@gmail.com`

---

### Printscreens

- Home page

![Home page](/screenshots/HomePage.jpg)

</br>

- Categories

![Categories](/screenshots/Categories.jpg)

</br>

- Quizzes

![Quizzes](/screenshots/Quizzes.jpg)

</br>

- Quiz

![Quiz](/screenshots/Quiz.jpg)

</br>

- Leaderboard

![Leaderboard](/screenshots/Leaderboard.jpg)

</br>

- Quiz History

![Quiz History](/screenshots/QuizHistory.jpg)

</br>
- Profile Page

![Profile Page](/screenshots/ProfilePage.jpg)
