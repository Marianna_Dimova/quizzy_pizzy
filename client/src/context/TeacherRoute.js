import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const TeacherRoute = ({ component: Component, auth, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => (auth.isTeacher ? <Component {...props} /> : <Redirect to='/teacher/dashboard/categories' />)}
    />
  );
};

export default TeacherRoute;
