import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const StudentRoute = ({ component: Component, auth, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => (!auth.isTeacher ? <Component {...props} /> : <Redirect to='/student/dashboard/categories' />)}
    />
  );
};

export default StudentRoute;
