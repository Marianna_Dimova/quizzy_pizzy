import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PublicRoute = ({ component: Component, auth, redirect, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => (auth && !redirect ? <Component {...props} /> : <Redirect to={redirect || '/'} />)}
    />
  );
};

export default PublicRoute;
