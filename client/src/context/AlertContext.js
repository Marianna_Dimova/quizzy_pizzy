import { createContext } from 'react';

const AlertContext = createContext({ alert: null, showAlert: () => {} });

export default AlertContext;
