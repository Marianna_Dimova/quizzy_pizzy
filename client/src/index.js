import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/scss/black-dashboard-react.scss';
import './assets/css/nucleo-icons.css';
import './index.css';

ReactDOM.render(<App />, document.getElementById('root'));
