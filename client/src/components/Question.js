import React, { useEffect, useState, useContext } from 'react';
import axios from '../requests/axios';
import { useLocation, useHistory, NavLink } from 'react-router-dom';
import { Row, Col, Card, Button, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { cloneDeep } from 'lodash';
import { getLoggedUser } from '../context/UserContext';
import api from '../api';
import AlertContext from '../context/AlertContext';
import CreateQuestion from './CreateQuestion';

const Questions = props => {
  const [user] = useState(getLoggedUser());
  const { showAlert } = useContext(AlertContext);
  const location = useLocation();
  const { id, quizzesId } = props.match.params;
  const [questions, setQuestion] = useState([]);
  const [finalScore, setFinalScore] = useState(null);
  const history = useHistory();

  const createQuiz = location.pathname === `/teacher/dashboard/categories/${id}/quizzes/${quizzesId}/questions/create`;
  const solveQuiz =
    location.pathname ===
    `/${user.isTeacher ? 'teacher' : 'student'}/dashboard/categories/${id}/quizzes/${quizzesId}/questions/solve`;

  useEffect(() => {
    createQuiz
      ? api.createdQuestions(quizzesId).then(data => setQuestion(data.questions))
      : api.questions(quizzesId).then(data => setQuestion(data.questions));
  }, [quizzesId]);

  const createQuestion = newQuestion => {
    api.createQuestion(newQuestion, quizzesId).then(data => {
      if (!questions.length) setQuestion([data]);
      else {
        setQuestion([...questions, data]);
      }
    });
  };

  const [answers, setAnswers] = useState(
    questions.reduce((acc, question) => {
      const { questionId } = question;
      return acc.set(questionId, { questionId, answer: '' });
    }, new Map())
  );

  const selectAnswer = (questionId, event) => {
    const newAnswers = cloneDeep(answers);
    newAnswers.set(questionId, {
      ...answers.get(questionId),
      questionId: questionId,
      questionAnswersId: event.target.value
    });
    setAnswers(newAnswers);
  };

  const setActive = () => {
    api
      .activateQuiz(id, quizzesId)
      .then(() => {
        history.push(`/teacher/dashboard/categories/${id}/quizzes`);
        showAlert({ msg: `You activate the quiz` });
      })
      .catch(e => {
        showAlert({ msg: JSON.parse(e.request.response).message, color: 'danger' });
      });
  };

  const SubmitAnswers = () => {
    axios
      .post(`questions/quiz/${quizzesId}/submit`, { answers: [...answers.values()] })
      .then(({ data }) => {
        setFinalScore(data.scores);
        history.push(`/${user.isTeacher ? 'teacher' : 'student'}/dashboard/categories`);
        showAlert({ msg: `Your score is ${data.scores} points!` });
      })
      .catch(e => {
        showAlert({ msg: JSON.parse(e.request.response).message, color: 'danger' });
      });
  };

  return (
    <div className='content'>
      <Row>
        <Col md='12'>
          <Breadcrumb>
            <BreadcrumbItem>
              <NavLink to={`/${user.isTeacher ? 'teacher' : 'student'}/dashboard/categories`}>Categories</NavLink>
            </BreadcrumbItem>
            <BreadcrumbItem>
              <NavLink to={`/${user.isTeacher ? 'teacher' : 'student'}/dashboard/categories/${id}/quizzes`}>
                Quizzes
              </NavLink>
            </BreadcrumbItem>
            <BreadcrumbItem active>Questions</BreadcrumbItem>
          </Breadcrumb>
        </Col>
        <Col md='12'>
          {createQuiz && <CreateQuestion create={createQuestion} />}
          <Card className='p-3'>
            {questions.map((question, index) => {
              return (
                <div key={question.question_id}>
                  <label htmlFor='questionText'>
                    {++index} : {question.question_text} - score : {question.score_individual_question}
                  </label>
                  <div type='text' name='questionText' id='questionText' />
                  <div type='text' name='scoreIndividualQuestion' id='scoreIndividualQuestion' />
                  <form>
                    <div className='d-flex'>
                      {question.answers.map(answer => {
                        return (
                          <div key={answer.question_answers_id}>
                            {solveQuiz && (
                              <input
                                type='radio'
                                name='question_answers_id'
                                value={answer.question_answers_id}
                                onChange={event => selectAnswer(question.question_id, event)}
                              />
                            )}
                            <label htmlFor='description'>{answer.answer}</label>
                            <div type='text' name='answer'></div>
                          </div>
                        );
                      })}
                    </div>
                  </form>
                </div>
              );
            })}
            {createQuiz ? (
              <Button color='info' onClick={() => setActive()}>
                Activate
              </Button>
            ) : (
              <Button color='primary' onClick={() => SubmitAnswers(finalScore)}>
                Submit
              </Button>
            )}
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default Questions;
