import React, { useEffect, useState } from 'react';
import { Table } from 'reactstrap';
import maleImg from '../assets/img/male.png';
import femaleImg from '../assets/img/female.png';
import api from '../api';

const Leaderboard = () => {
  const [leaderBoard, setLeaderBoard] = useState([]);

  useEffect(() => {
    api.leaderBoard().then(data => {
      setLeaderBoard(data.leaderBoard);
    });
  }, []);

  return (
    <div className='content'>
      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>User</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Score</th>
          </tr>
        </thead>
        <tbody>
          {leaderBoard.map((person, index) => {
            return (
              <tr key={index}>
                <td style={{ width: '40px' }}>{++index}</td>
                <td>
                  <img
                    style={{ height: '30px', marginRight: '1rem' }}
                    alt='...'
                    src={person?.gender === 'm' ? maleImg : femaleImg}
                  />
                  {person.username}
                </td>
                <td>{person.firstName}</td>
                <td>{person.lastName}</td>
                <td>{person.score}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
};

export default Leaderboard;
