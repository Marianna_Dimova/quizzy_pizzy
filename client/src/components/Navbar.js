import React, { useState, useContext } from 'react';
import {
  Collapse,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  NavbarBrand,
  Navbar as ReactstrapNavbar,
  NavLink,
  Nav,
  Container
} from 'reactstrap';
import classNames from 'classnames';
import { useHistory } from 'react-router-dom';
import maleImg from '../assets/img/male.png';
import femaleImg from '../assets/img/female.png';
import UserContext from '../context/UserContext';

const Navbar = () => {
  const history = useHistory();
  const [color] = useState('navbar-transparent');
  const [toggled] = useState(true);
  const { user, setUser } = useContext(UserContext);

  const viewDashboard = () => {
    history.push(`/${user.isTeacher ? 'teacher' : 'student'}/dashboard/categories`);
  };

  const viewProfile = () => {
    history.push('/profile');
  };

  const logout = () => {
    setUser(null);
    localStorage.removeItem('token');
    history.push('/');
  };

  return (
    <ReactstrapNavbar className={classNames('navbar-absolute', color)} expand='lg'>
      <Container fluid>
        <div className='navbar-wrapper'>
          <div className={classNames('navbar-toggle d-inline', { toggled })}>
            <button className='navbar-toggler' type='button' onClick={() => {}}>
              <span className='navbar-toggler-bar bar1' />
              <span className='navbar-toggler-bar bar2' />
              <span className='navbar-toggler-bar bar3' />
            </button>
          </div>
          <NavbarBrand className='btn btn-link text-light' onClick={viewDashboard}>
            Dashboard
          </NavbarBrand>
        </div>
        <button
          aria-expanded={false}
          aria-label='Toggle navigation'
          className='navbar-toggler'
          data-target='#navigation'
          data-toggle='collapse'
          id='navigation'
          type='button'
          onClick={() => {}}>
          <span className='navbar-toggler-bar navbar-kebab' />
          <span className='navbar-toggler-bar navbar-kebab' />
          <span className='navbar-toggler-bar navbar-kebab' />
        </button>
        <Collapse navbar isOpen={false}>
          <Nav className='ml-auto' navbar>
            <UncontrolledDropdown nav>
              <DropdownToggle caret color='default' data-toggle='dropdown' nav onClick={e => e.preventDefault()}>
                <span style={{ fontSize: '1.05em', marginRight: '1rem' }}>
                  {user.firstName} {user.lastName}
                </span>
                <div className='photo'>
                  <img alt='...' src={user?.gender === 'm' ? maleImg : femaleImg} />
                </div>
                <p className='d-lg-none'>Log out</p>
              </DropdownToggle>
              <DropdownMenu className='dropdown-navbar' right tag='ul'>
                <NavLink tag='li'>
                  <DropdownItem className='nav-item' onClick={viewProfile}>
                    Profile
                  </DropdownItem>
                </NavLink>
                <DropdownItem divider tag='li' />
                <NavLink tag='li'>
                  <DropdownItem className='nav-item' onClick={logout}>
                    Log out
                  </DropdownItem>
                </NavLink>
              </DropdownMenu>
            </UncontrolledDropdown>
            <li className='separator d-lg-none' />
          </Nav>
        </Collapse>
      </Container>
    </ReactstrapNavbar>
  );
};

export default Navbar;
