import React, { useRef } from 'react';
import { Nav } from 'reactstrap';

const Sidebar = ({ bgColor, children }) => {
  const ref = useRef();

  return (
    <div className='sidebar' data={bgColor}>
      <div className='sidebar-wrapper' ref={ref}>
        <h4 className='logo'>
          <i className='tim-icons icon-bulb-63 pb-1 pr-1 text-white' /> Quiz App
        </h4>
        <Nav>{children}</Nav>
      </div>
    </div>
  );
};

export default Sidebar;
