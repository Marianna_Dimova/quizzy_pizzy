import React, { useState } from 'react';
import { FormGroup, Label, Input, Form, Button, Row, Col } from 'reactstrap';

const CreateQuestion = prop => {
  const blankQuestion = {
    question_text: '',
    score_individual_question: ''
  };
  const [questionState, setQuestionState] = useState({ ...blankQuestion });

  const blankAnswer = { answer: '', is_right: 0 };
  const [answerState, setAnswerState] = useState([{ ...blankAnswer }]);
  const [EveryThing, setEveryThing] = useState({
    ...questionState,
    answers: [...answerState]
  });

  const handleQuestionChange = e => {
    const updatedQuestion = { ...questionState };
    updatedQuestion[e.target.name] = e.target.value;
    setQuestionState(updatedQuestion);
    setEveryThing({ ...EveryThing, ...updatedQuestion });
  };

  const addAnswer = () => {
    setAnswerState([...answerState, { ...blankAnswer }]);
  };

  const handleAnswerChange = (key, idx) => ({ target: { value } }) => {
    const answers = answerState.map(el => ({ ...el, is_right: 0 }));
    const answer = answers[idx];
    answer[key] = value;
    answers[idx] = answer;

    setAnswerState(answers);
    setEveryThing({ ...EveryThing, answers: [...answers] });
  };

  const create = () => {
    prop.create(EveryThing);
    setQuestionState({ ...blankQuestion });
    setAnswerState([{ ...blankAnswer }]);
  };

  return (
    <Form className='mb-4'>
      <Row>
        <Col md='6'>
          <Label htmlFor='questionText'>Question</Label>
          <Input
            type='text'
            name='question_text'
            id='question_text'
            value={questionState.question_text}
            onChange={handleQuestionChange}
          />
        </Col>
        <Col md='6'>
          <Label for='description'>Score</Label>
          <Input
            type='select'
            name='score_individual_question'
            id='exampleSelect'
            value={questionState.score_individual_question}
            onChange={handleQuestionChange}>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
          </Input>
        </Col>

        <Col md='12'>
          {answerState.map((val, idx) => {
            const answerId = `answer-${idx}`;
            const isTrueId = `is_right-${idx}`;
            return (
              <Row key={`answer-${idx}`}>
                <Col md='11'>
                  <Label htmlFor={answerId}>{`Answer #${idx + 1}`}</Label>
                  <Input
                    type='text'
                    name={answerId}
                    data-idx={idx}
                    id={answerId}
                    className='answer'
                    value={val.answer}
                    onChange={handleAnswerChange('answer', idx)}
                  />
                </Col>
                <Col style={{ width: '20px' }} className='pr-0'>
                  <FormGroup check>
                    <Label check>
                      <Input
                        style={{ display: 'block', height: '38px', marginTop: '14px', marginLeft: '14px' }}
                        type='radio'
                        name='is_right'
                        data-idx={idx}
                        id={isTrueId}
                        className='is_right'
                        value={1}
                        onChange={handleAnswerChange('is_right', idx)}
                      />
                      Correct
                    </Label>
                  </FormGroup>
                </Col>
              </Row>
            );
          })}
          <div className='d-flex justify-content-between'>
            <Button className='py-2 px-3 mt-3' color='warning' onClick={addAnswer}>
              Add Answer
            </Button>
            <Button
              style={{ width: '150px' }}
              className='py-2 px-3 mt-3'
              color='primary'
              onClick={() => {
                create(EveryThing);
              }}>
              Submit
            </Button>
          </div>
        </Col>
      </Row>
    </Form>
  );
};

export default CreateQuestion;
