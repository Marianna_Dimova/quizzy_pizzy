import React, { useEffect, useState } from 'react';
import { Table } from 'reactstrap';
import api from '../api';

const QuizHistory = () => {
  const [history, setHistory] = useState([]);

  useEffect(() => {
    api.quizHistory().then(data => {
      setHistory(data);
    });
  }, []);

  return (
    <div className='content'>
      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>Quiz name</th>
            <th>Started</th>
            <th>Finish</th>
          </tr>
        </thead>
        <tbody>
          {history.map((quiz, index) => {
            return (
              <tr key={index}>
                <td style={{ width: '40px' }}>{++index}</td>
                <td>{quiz.quiz_name}</td>
                <td>{quiz.started_at}</td>
                <td>{quiz.finished_at}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
};

export default QuizHistory;
