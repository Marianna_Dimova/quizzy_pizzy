import React from 'react';

const AnswerForm = ({ idx, answerState, handleAnswerChange }) => {
  const answerId = `answer-${idx}`;
  const isTrueId = `is_right-${idx}`;
  return (
    <div key={`answer-${idx}`}>
      <label htmlFor={answerId}>{`Answer #${idx + 1}`}</label>
      <input
        type='text'
        name={answerId}
        data-idx={idx}
        id={answerId}
        className='answer'
        value={answerState[idx].answer}
        onChange={handleAnswerChange}
      />
      <label htmlFor={isTrueId}>Is it true ?</label>
      <input
        type='radio'
        name={isTrueId}
        data-idx={idx}
        id={isTrueId}
        className='is_right'
        value={1}
        onChange={handleAnswerChange}
      />
    </div>
  );
};

export default AnswerForm;
