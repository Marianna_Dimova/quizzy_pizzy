import axios from 'axios';
export const baseUrl = 'http://localhost:3000';

const request = (method, url, data, id) => {
  return axios({
    baseURL: 'http://localhost:3000',
    headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
    url,
    method,
    data
  }).then(({ data }) => data);
};

export default {
  register: newUserCredentials => request('POST', '/users', newUserCredentials),
  login: userCredentials => request('POST', '/auth/login', userCredentials),
  categories: () => request('GET', '/categories'),
  createCategory: category => request('POST', '/categories', category),
  quizzes: id => request('GET', `/categories/${id}/quizzes`),
  createQuiz: (id, quiz) => request('POST', `/categories/${id}/quizzes`, quiz),
  questions: id => request('GET', `questions/start/quiz/${id}`),
  createQuestion: (question, id) => request('POST', `questions/${id}/create`, question),
  leaderBoard: () => request('GET', `questions/leader_board`),
  activateQuiz: (id, quizId) => request('PUT', `categories/${id}/quizzes/${quizId}/status`),
  quizHistory: () => request('GET', `categories/quizzes/history`),
  createdQuestions: quizId => request('GET', `questions/createQuestions/${quizId}`)
};
