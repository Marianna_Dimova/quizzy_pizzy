import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  FormGroup,
  Form,
  Input,
  Row,
  Col,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  ButtonGroup,
  UncontrolledTooltip
} from 'reactstrap';
import classnames from 'classnames';
import jwtDecode from 'jwt-decode';
import UserContext from '../context/UserContext';
import AlertContext from '../context/AlertContext';
import maleImg from '../assets/img/male.png';
import femaleImg from '../assets/img/female.png';
import api from '../api';

const HomePage = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [activeTab, setActiveTab] = useState('Login');

  const { setUser } = useContext(UserContext);
  const { showAlert } = useContext(AlertContext);

  const [userCredentials, setUserCredentials] = useState({
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    gender: 'f'
  });

  const validationFeedback = [
    !userCredentials.username ? 'Username is required' : '',
    !userCredentials.password ? 'Password is required \n' : '',
    activeTab === 'Register' && !userCredentials.firstName ? 'First Name is required \n' : '',
    activeTab === 'Register' && !userCredentials.lastName ? 'Last Name is required \n' : ''
  ];

  const updateGender = gender => () => setUserCredentials({ ...userCredentials, gender });
  const updateUsername = username => setUserCredentials({ ...userCredentials, username });
  const updatePassword = password => setUserCredentials({ ...userCredentials, password });

  const updateNewUsername = username => setUserCredentials({ ...userCredentials, username });
  const updateNewPassword = password => setUserCredentials({ ...userCredentials, password });
  const updateNewFirstName = firstName => setUserCredentials({ ...userCredentials, firstName });
  const updateNewLastName = lastName => setUserCredentials({ ...userCredentials, lastName });

  const login = () => {
    setLoading(true);
    api
      .login(userCredentials)
      .then(({ token }) => {
        setUser(jwtDecode(token));
        localStorage.setItem('token', token);
        showAlert({ msg: 'You have logged in successfully.' });
        jwtDecode(token).isTeacher
          ? history.push('/teacher/dashboard/categories')
          : history.push('/student/dashboard/categories');

        setLoading(false);
      })
      .catch(e => {
        showAlert({ msg: JSON.parse(e.request.response).message, color: 'danger' });
      });
  };

  const register = () => {
    setLoading(true);
    api
      .register(userCredentials)
      .then(login)
      .catch(e => {
        showAlert({ msg: JSON.parse(e.request.response).message, color: 'danger' });
      });
  };

  const onSubmit = ev => {
    ev.preventDefault();
    activeTab === 'Login' ? login() : register();
  };

  const selectTab = tab => () => {
    setUserCredentials({ username: '', password: '', firstName: '', lastName: '', gender: 'f' });
    setActiveTab(tab);
  };

  return (
    <Col md='4' className='mx-auto'>
      <Nav tabs style={{ paddingTop: '10rem' }}>
        <NavItem>
          <NavLink className={classnames({ active: activeTab === 'Login' })} onClick={selectTab('Login')}>
            Login
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink className={classnames({ active: activeTab === 'Register' })} onClick={selectTab('Register')}>
            Register
          </NavLink>
        </NavItem>
      </Nav>
      <Card>
        <CardBody className='py-4'>
          <Form>
            <TabContent activeTab={activeTab}>
              <TabPane tabId='Login'>
                <Row>
                  <Col className='px-4' md='12'>
                    <FormGroup>
                      <label>Username</label>
                      <Input
                        type='text'
                        className='form-control'
                        placeholder='Username...'
                        value={userCredentials.username}
                        onChange={e => updateUsername(e.target.value)}
                      />
                    </FormGroup>
                  </Col>
                  <Col className='px-4' md='12'>
                    <FormGroup>
                      <label>Password</label>
                      <Input
                        type='password'
                        className='form-control'
                        placeholder='Password...'
                        value={userCredentials.password}
                        onChange={e => updatePassword(e.target.value)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='Register'>
                <Row>
                  <Col className='text-center p-4' md='4'>
                    <Row>
                      <Col md='12' className='text-center'>
                        <img
                          style={{ minWidth: '180px', height: '220px' }}
                          alt='...'
                          src={!userCredentials || userCredentials.gender === 'm' ? maleImg : femaleImg}
                        />
                      </Col>
                      <Col md='12' className='text-center mt-2'>
                        <ButtonGroup>
                          <Button
                            className='py-2'
                            style={{ minWidth: '90px', margin: 0 }}
                            size='sm'
                            color='info'
                            onClick={updateGender('m')}
                            active={userCredentials.gender === 'm'}>
                            Male
                          </Button>
                          <Button
                            className='py-2'
                            style={{ minWidth: '90px', margin: 0 }}
                            size='sm'
                            color='danger'
                            onClick={updateGender('f')}
                            active={userCredentials.gender === 'f'}>
                            Female
                          </Button>
                        </ButtonGroup>
                      </Col>
                    </Row>
                  </Col>

                  <Col md='8'>
                    <Row>
                      <Col className='px-4' md='12'>
                        <FormGroup>
                          <label>Username</label>
                          <Input
                            type='text'
                            placeholder='Username...'
                            value={userCredentials.username}
                            onChange={e => updateNewUsername(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                      <Col className='px-4' md='12'>
                        <FormGroup>
                          <label>First Name</label>
                          <Input
                            type='text'
                            placeholder='First Name...'
                            value={userCredentials.firstName}
                            onChange={e => updateNewFirstName(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                      <Col className='px-4' md='12'>
                        <FormGroup>
                          <label>Last Name</label>
                          <Input
                            type='text'
                            placeholder='Last Name...'
                            value={userCredentials.lastName}
                            onChange={e => updateNewLastName(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                      <Col className='px-4' md='12'>
                        <FormGroup>
                          <label>Password</label>
                          <Input
                            type='password'
                            placeholder='Password...'
                            value={userCredentials.password}
                            onChange={e => updateNewPassword(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </Form>
        </CardBody>
        <CardFooter className='text-right pr-4 pt-0'>
          <span id='submit'>
            <Button
              style={{ width: '120px' }}
              className={`btn-fill py-2 ${validationFeedback.join('').length > 0 ? 'disabled' : ''}`}
              size='sm'
              color='primary'
              type='submit'
              onClick={onSubmit}>
              {activeTab}
            </Button>
          </span>
          {validationFeedback.join('').length > 0 && (
            <UncontrolledTooltip placement='left' target='submit'>
              {validationFeedback.map((msg, index) => (
                <div key={index}>{msg}</div>
              ))}
            </UncontrolledTooltip>
          )}
        </CardFooter>
      </Card>
    </Col>
  );
};

export default HomePage;
