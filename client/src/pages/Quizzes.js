import React, { useState, useEffect, useContext } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import {
  Card,
  Input,
  CardText,
  UncontrolledTooltip,
  Button,
  CardHeader,
  CardBody,
  Row,
  Col,
  Breadcrumb,
  BreadcrumbItem,
  Modal,
  ModalBody
} from 'reactstrap';
import api from '../api.js';
import AlertContext from '../context/AlertContext';
import UserContext from '../context/UserContext';

const Quizzes = prop => {
  const [modal, setModal] = useState({ show: false, quiz: null });
  const [quizHistory, setQuizHistory] = useState([]);
  const { user } = useContext(UserContext);
  const history = useHistory();
  const { id } = prop.match.params;
  const [quizName, setQuizName] = useState('');
  const [requiredTime, setRequiredTime] = useState('');
  const [quizzes, setQuizzes] = useState([]);
  const { showAlert } = useContext(AlertContext);

  useEffect(() => {
    api.quizHistory().then(historyData => {
      setQuizHistory(historyData);
      api.quizzes(id).then(quizData => {
        setQuizzes(
          quizData
            .map(quiz => {
              if (historyData.some(q => q.quiz_id === quiz.quizId)) {
                return { ...quiz, disabled: true };
              }

              return quiz;
            })
            .sort((a, b) => (a.disabled || b.disabled ? -1 : 0))
        );
      });
    });
  }, [id]);

  const create = () => {
    api
      .createQuiz(id, { quizName, requiredTime })
      .then(data => {
        if (!quizzes.length) {
          setQuizzes([data]);
          showAlert({ msg: 'Quiz created successfully.' });
        } else {
          setQuizzes([...quizzes, data]);
          showAlert({ msg: 'Quiz created successfully.' });
        }
      })
      .catch(e => {
        showAlert({ msg: JSON.parse(e.request.response).message, color: 'danger' });
      });
  };

  const usersQuizzes = quizzes.filter(el => el.isActive);
  const validationFeedback = [!quizName ? 'Quiz Name is required' : '', !requiredTime ? 'Quiz Time is required' : ''];
  return (
    <div className='content'>
      <Modal
        style={{ width: '360px', textAlign: 'center' }}
        isOpen={modal.show}
        toggle={() => {
          setModal({ show: !modal.show, quiz: null });
        }}>
        <ModalBody>
          <h3 className='text-center text-black-50'>Start Quiz?</h3>
          <div>
            <Button
              style={{ width: '100px' }}
              className='py-2 mr-2'
              size='sm'
              color='primary'
              onClick={() => {
                history.push(
                  `/${user.isTeacher ? 'teacher' : 'student'}/dashboard/categories/${modal.quiz.categoryId}/quizzes/${
                    modal.quiz.quizId
                  }/questions/solve`
                );
              }}>
              Yes
            </Button>
            <Button
              style={{ width: '100px' }}
              className='py-2 ml-2'
              size='sm'
              color='secondary'
              onClick={() => {
                setModal({ show: false, quiz: null });
              }}>
              No
            </Button>
          </div>
        </ModalBody>
      </Modal>
      <Row>
        <Col md='12'>
          <Breadcrumb>
            <BreadcrumbItem>
              <NavLink to={`/${user.isTeacher ? 'teacher' : 'student'}/dashboard/categories`}>Categories</NavLink>
            </BreadcrumbItem>
            <BreadcrumbItem active>Quizzes</BreadcrumbItem>
          </Breadcrumb>
        </Col>
        {user.isTeacher === 1 && (
          <Col md='12'>
            <Card>
              <CardHeader>
                <h4 className='title'>Create new Quiz</h4>
              </CardHeader>
              <CardBody>
                <Row className='justify-content-end align-items-end'>
                  <Col md='12'>
                    <div style={{ display: 'flex', alignItems: 'flex-end' }}>
                      <div style={{ width: 'calc(50% - 60px)', paddingRight: '14px' }}>
                        <CardText>Quiz Name:</CardText>
                        <Input value={quizName} onChange={ev => setQuizName(ev.target.value)} />
                      </div>
                      <div style={{ width: 'calc(50% - 60px)', paddingRight: '14px' }}>
                        <CardText>Quiz Time:</CardText>
                        <Input value={requiredTime} onChange={ev => setRequiredTime(ev.target.value)} />
                      </div>
                      <div>
                        <span className='pt-2' id='submit'>
                          <Button
                            style={{ minWidth: '120px', height: '38px' }}
                            className={`mb-0 py-1 ${validationFeedback.join('').length > 0 ? 'disabled' : ''}`}
                            size='sm'
                            color='primary'
                            type='submit'
                            onClick={create}>
                            Create
                          </Button>
                        </span>
                        {validationFeedback.join('').length > 0 && (
                          <UncontrolledTooltip placement='left' target='submit'>
                            {validationFeedback.map((msg, index) => (
                              <div key={index}>{msg}</div>
                            ))}
                          </UncontrolledTooltip>
                        )}
                      </div>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        )}

        <Col>
          <Row>
            <Col md='12'>
              <Card>
                <CardHeader>
                  <h4 className='mb-0'>{quizzes.length} Quizzes</h4>
                </CardHeader>
                <CardBody>
                  <Row className='d-flex flex-wrap'>
                    {(user.isTeacher ? quizzes : usersQuizzes).map((quiz, index) => (
                      <Col
                        key={index}
                        style={{ maxWidth: '100%' }}
                        className='font-icon-list py-0'
                        lg='2'
                        md='3'
                        sm='4'>
                        <div style={{ fontSize: '1.1em' }}>
                          <div className='font-icon-detail quiz pb-3'>
                            <div className='mb-4'>
                              <i className='tim-icons icon-single-copy-04 pr-1' style={{ fontSize: '2em' }} />
                            </div>
                            {quiz.quizName}
                            <div className='pt-3'>
                              <Button
                                onClick={() => {
                                  setModal({ show: true, quiz });
                                }}
                                disabled={!quiz.isActive || (quiz.disabled && !user.isTeacher)}
                                style={{ width: quiz.isActive || !Boolean(user.isTeacher) ? '200px' : '135px' }}
                                size='sm'
                                color='warning'>
                                Start Quiz
                              </Button>
                              {!quiz.isActive && Boolean(user.isTeacher) && (
                                <Button
                                  onClick={() => {
                                    history.push(
                                      `/${user.isTeacher ? 'teacher' : 'student'}/dashboard/categories/${
                                        quiz.categoryId
                                      }/quizzes/${quiz.quizId}/questions/create`
                                    );
                                  }}
                                  style={{ width: '135px' }}
                                  size='sm'
                                  color='info'>
                                  Add Questions
                                </Button>
                              )}
                            </div>
                          </div>
                        </div>
                      </Col>
                    ))}
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default Quizzes;
