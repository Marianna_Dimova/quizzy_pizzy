import React, { useRef, useState } from 'react';
import { Route, Switch, NavLink } from 'react-router-dom';
import Sidebar from '../components/Sidebar.js';
import Settings from '../components/Settings.js';
import Quizzes from './Quizzes.js';
import Leaderboard from '../components/Leaderboard.js';
import Categories from './Categories.js';
import Questions from '../components/Question.js';
import QuizHistory from '../components/QuizHistory.js';

const TeacherPage = props => {
  const ref = useRef();
  const [backgroundColor, setBackgroundColor] = useState('blue');

  const handleBgClick = color => {
    setBackgroundColor(color);
  };

  return (
    <>
      <div className='wrapper'>
        <Sidebar {...props} bgColor={backgroundColor} toggleSidebar={() => {}}>
          <li className={'active-pro'}>
            <NavLink to={'/teacher/dashboard/categories'} className='nav-link' activeClassName='active'>
              Categories
            </NavLink>
          </li>
          <li className={'active-pro'}>
            <NavLink
              to={'/teacher/dashboard/leaderboard'}
              className='nav-link'
              activeClassName='active'
              onClick={() => {}}>
              Leaderboard
            </NavLink>
          </li>
          <li className={'active-pro'}>
            <NavLink
              to={'/teacher/dashboard/quiz-history'}
              className='nav-link'
              activeClassName='active'
              onClick={() => {}}>
              History
            </NavLink>
          </li>
        </Sidebar>
        <div className='main-panel' ref={ref} data={backgroundColor}>
          <Switch>
            <Route path='/teacher/dashboard/categories' exact component={Categories} />
            <Route path='/teacher/dashboard/categories/:id/quizzes' exact component={Quizzes} />
            <Route
              path='/teacher/dashboard/categories/:id/quizzes/:quizzesId/questions/create'
              exact
              component={Questions}
            />
            <Route
              path='/teacher/dashboard/categories/:id/quizzes/:quizzesId/questions/solve'
              exact
              component={Questions}
            />
            <Route path='/teacher/dashboard/leaderboard' exact component={Leaderboard} />
            <Route path='/teacher/dashboard/quiz-history' exact component={QuizHistory} />
          </Switch>
        </div>
      </div>
      <Settings bgColor={backgroundColor} handleBgClick={handleBgClick} />
    </>
  );
};

export default TeacherPage;
