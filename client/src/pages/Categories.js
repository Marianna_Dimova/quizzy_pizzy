import React, { useState, useEffect, useContext } from 'react';
import { NavLink } from 'react-router-dom';
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
  Input,
  CardText,
  Button,
  Spinner,
  UncontrolledTooltip
} from 'reactstrap';
import AlertContext from '../context/AlertContext';
import api from '../api';
import UserContext from '../context/UserContext';

const Categories = () => {
  const { user } = useContext(UserContext);
  const [loading, setLoading] = useState(false);
  const [categories, setCategories] = useState([]);
  const { showAlert } = useContext(AlertContext);

  useEffect(() => {
    api.categories().then(data => {
      setCategories(data);
    });
  }, []);

  const [name, setName] = useState('');

  const create = () => {
    setLoading(true);
    api
      .createCategory({ name })
      .then(category => {
        setCategories([...categories, category]);
        setLoading(false);
        setName('');
        showAlert({ msg: 'Category created successfully.' });
      })
      .catch(e => {
        showAlert({ msg: JSON.parse(e.request.response).message, color: 'danger' });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const validationFeedback = !name ? 'Category Name is required' : '';

  return (
    <div className='content'>
      {Boolean(user.isTeacher) && (
        <Row>
          <Col md='12'>
            <Card>
              <CardHeader>
                <h4 className='title'>Create new Category</h4>
              </CardHeader>
              <CardBody>
                <Col className='px-0' md='12'>
                  <div style={{ display: 'flex', alignItems: 'flex-end' }}>
                    <div style={{ width: 'calc(100% - 120px)', paddingRight: '14px' }}>
                      <CardText className='font-weight-bold'>Category Name:</CardText>
                      <Input value={name} onChange={ev => setName(ev.target.value)} />
                    </div>
                    <div>
                      {loading ? (
                        <Spinner color='primary' />
                      ) : (
                        <span className='pt-2' id='submit'>
                          <Button
                            color='primary'
                            style={{ minWidth: '120px', height: '38px' }}
                            className={`mb-0 py-1 ${validationFeedback.length > 0 ? 'disabled' : ''}`}
                            size='sm'
                            onClick={create}>
                            Create
                          </Button>
                        </span>
                      )}
                      {validationFeedback.length > 0 && (
                        <UncontrolledTooltip placement='top' target='submit'>
                          {validationFeedback}
                        </UncontrolledTooltip>
                      )}
                    </div>
                  </div>
                </Col>
              </CardBody>
            </Card>
          </Col>
        </Row>
      )}
      <Row>
        <Col md='12'>
          <Card>
            <CardHeader>
              <h4 className='mb-0'>{categories.length} Categories</h4>
            </CardHeader>
            <CardBody>
              <Row>
                {categories.map((category, index) => (
                  <Col style={{ maxWidth: '100%' }} key={index} className='font-icon-list py-0' lg='2' md='3' sm='4'>
                    <NavLink
                      style={{ fontSize: '1.1em' }}
                      to={`/${user.isTeacher ? 'teacher' : 'student'}/dashboard/categories/${
                        category.categoryId || category.id
                      }/quizzes`}>
                      <div className='font-icon-detail'>
                        <div className='mb-4'>
                          <i className='tim-icons icon-paper pr-1' style={{ fontSize: '2em' }} />
                        </div>
                        {category.categoryName}
                      </div>
                    </NavLink>
                  </Col>
                ))}
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default Categories;
