import React, { useState } from 'react';
import { Button, Card, CardBody, CardFooter, CardText, Col } from 'reactstrap';
import maleImg from '../assets/img/male.png';
import femaleImg from '../assets/img/female.png';
import { getLoggedUser } from '../context/UserContext';

const ProfilePage = () => {
  const [user] = useState(getLoggedUser());

  return (
    <Col style={{ paddingTop: '10rem' }} md='4' className='mx-auto'>
      <Card className='card-user'>
        <CardBody>
          <CardText />
          <div className='author'>
            <div className='block block-one' />
            <div className='block block-two' />
            <div className='block block-three' />
            <div className='block block-four' />
            <a href='#pablo' onClick={e => e.preventDefault()}>
              <img
                style={{ width: '180px', height: 'auto' }}
                alt='...'
                className='avatar'
                src={user?.gender === 'm' ? maleImg : femaleImg}
              />
              <h5 className='title'>
                {user.firstName} {user.lastName}
              </h5>
            </a>
            <p className='description'>{user.isTeacher ? 'Teacher' : 'Student'}</p>
          </div>
        </CardBody>
        <CardFooter>
          <div className='button-container'>
            <Button className='btn-icon btn-round' color='facebook'>
              <i className='fab fa-facebook' />
            </Button>
            <Button className='btn-icon btn-round' color='twitter'>
              <i className='fab fa-twitter' />
            </Button>
            <Button className='btn-icon btn-round' color='google'>
              <i className='fab fa-google-plus' />
            </Button>
          </div>
        </CardFooter>
      </Card>
    </Col>
  );
};

export default ProfilePage;
