import React, { useState } from 'react';
import { Router, Route, Switch, BrowserRouter } from 'react-router-dom';
import { Alert } from 'reactstrap';
import { createBrowserHistory } from 'history';
import HomePage from './pages/HomePage';
import TeacherPage from './pages/TeacherPage';
import ProfilePage from './pages/ProfilePage';
import Navbar from './components/Navbar';
import UserContext, { getLoggedUser } from './context/UserContext';
import AlertContext from './context/AlertContext';
import PublicRoute from './context/PublicRoute';
import TeacherRoute from './context/TeacherRoute';
import StudentRoute from './context/StudentRoute';
import StudentPage from './pages/StudentPage';

const history = createBrowserHistory();

const App = () => {
  const [user, setUser] = useState(getLoggedUser());
  const [alert, setAlert] = useState(null);

  const showAlert = ({ msg, color = 'success', timeout = 2000 }) => {
    setAlert({ msg, color, timeout });
    setTimeout(() => {
      setAlert(null);
    }, timeout);
  };

  return (
    <BrowserRouter>
      <AlertContext.Provider value={{ showAlert }}>
        <UserContext.Provider value={{ user, setUser }}>
          {alert && alert.msg && (
            <Alert color={alert?.color} className='alert'>
              {alert?.msg}
            </Alert>
          )}
          <Router history={history}>
            {user && <Navbar />}
            <Switch>
              <PublicRoute
                path='/'
                auth={!user}
                redirect={user ? `/${user.isTeacher ? 'teacher' : 'student'}/dashboard/categories` : ''}
                exact
                component={HomePage}
              />
              <TeacherRoute path='/teacher' auth={user} component={TeacherPage} />
              <StudentRoute path='/student' auth={user} component={StudentPage} />
              <Route path='/profile' component={ProfilePage} />
              <Route path='*' render={() => <h2 className='text-center pt-5'>Not Found</h2>} />
            </Switch>
          </Router>
        </UserContext.Provider>
      </AlertContext.Provider>
    </BrowserRouter>
  );
};

export default App;
