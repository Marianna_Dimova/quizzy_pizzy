import pool from './pool.js';

const getAll = async () => {
  const sql = `
        SELECT user_id, username,first_name,last_name, is_teacher, gender
        FROM users
    `;

  return await pool.query(sql);
};

const getWithRole = async username => {
  const sql = `
        SELECT user_id, username, password,first_name, last_name, is_teacher, gender
        FROM users
        WHERE username = ?
    `;

  const [user] = await pool.query(sql, [username]);

  return user;
};

const getBy = async (column, value) => {
  const sql = `
        SELECT user_id, username,first_name,last_name, is_teacher, gender
        FROM users
        WHERE ${column} = ?
    `;

  const [user] = await pool.query(sql, [value]);

  return user;
};

const searchBy = async (column, value) => {
  const sql = `
        SELECT user_id, username, first_name, last_name, is_teacher, gender
        FROM users
        WHERE ${column} LIKE '%${value}%' 
    `;

  return await pool.query(sql);
};

const create = async (username, password, firstName, lastName, isTeacher, gender) => {
  const sql = `
        INSERT INTO users(username, password,first_name, last_name, is_teacher, gender)
        VALUES (?,?,?,?,?,?)
    `;

  const { insertId: userId } = await pool.query(sql, [username, password, firstName, lastName, isTeacher, gender]);

  return { userId, username, firstName, lastName, isTeacher, gender };
};

export default {
  getAll,
  searchBy,
  getBy,
  create,
  getWithRole
};
