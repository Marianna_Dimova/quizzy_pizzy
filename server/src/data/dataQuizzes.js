import pool from './pool.js';

const searchByQuiz = async (column, value) => {
  const sql = `
        SELECT quiz_id, quiz_name,required_time, category_id, creator_id, is_active
        FROM quizzes
        WHERE ${column} LIKE '%${value}%' 
    `;
  return await pool.query(sql);
};

const getByInQuiz = async (column, value) => {
  const sql = `
        SELECT quiz_id, quiz_name,required_time, category_id, creator_id, is_active
        FROM quizzes
        WHERE ${column} = ?
    `;
  const [quiz] = await pool.query(sql, [value]);

  return quiz;
};

const getQuizByIdAndCategoryId = async (id, categoryId) => {
  const sql = `
        SELECT * FROM quizzes WHERE quiz_id=? AND category_id = ?;
    `;
  const [categoryWithQuiz] = await pool.query(sql, [id, categoryId]);

  return categoryWithQuiz;
};

const getAllQuizzesInCurrentCategory = async (id, userId) => {
  const sql = `
    SELECT q.quiz_id, q.quiz_name, q.required_time, c.category_id, c.category_name, q.creator_id, q.is_active
        FROM quizzes as q
        LEFT JOIN categories as c ON q.category_id = c.category_id
        WHERE q.category_id = ${id}
    `;
  const categoryWithAllQuizzes = await pool.query(sql, [userId]);

  return categoryWithAllQuizzes;
};

const createQuiz = async (quizName, requiredTime, categoryId, creatorId) => {
  const sql = `
    INSERT INTO 
    quizzes(quiz_name, required_time, category_id, creator_id) 
    VALUES (?,?,?,?)
    `;

  const { insertId: quizId } = await pool.query(sql, [quizName, requiredTime, categoryId, creatorId]);

  return { quizId, quizName, requiredTime, categoryId, creatorId };
};

const createQuestion = async (questionText, scoreIndividualQuestion, quizId) => {
  const sql = `
    INSERT INTO 
    questions(question_text, score_individual_question, quiz_id) 
    VALUES (?,?,?)
    `;

  const { insertId: id } = await pool.query(sql, [questionText, scoreIndividualQuestion, quizId]);

  return { id, questionText, scoreIndividualQuestion, quizId };
};

const getQuestionByQuizId = async quizId => {
  const sql = `
    SELECT * FROM questions where quiz_id = ?;
    `;

  return await pool.query(sql, [quizId]);
};

const getQuestionAnswersByQuestionId = async questionId => {
  const sql = `
    SELECT question_answers_id, answer, is_right
    FROM question_answers
    WHERE question_id = ?;
    `;

  return await pool.query(sql, [questionId]);
};

// available answers set by teacher
const availableAnswers = async (questionAnswer, isRight, questionId) => {
  const sql = `
    INSERT INTO 
    question_answers(answer, is_right, question_id) 
    VALUES (?,?,?)
    `;
  const { insertId: id } = await pool.query(sql, [questionAnswer, isRight, questionId]);

  return { id, questionAnswer, isRight, questionId };
};
// insert user answers
const userAnswers = async (questionId, questionAnswersId, userId) => {
  const sql = `
    INSERT INTO
    user_question_answer
    (question_id, question_answers_id, user_id)
    VALUES (?,?,?)
    `;

  const { insertId: id } = await pool.query(sql, [questionId, questionAnswersId, userId]);

  return { id, questionId, questionAnswersId, userId };
};

const answerExists = async (questionId, userId) => {
  const sql = `
    SELECT COUNT(u.question_id) result
    FROM user_question_answer u
    WHERE u.question_id = ? 
    AND u.user_id=?
    `;
  const [answers] = await pool.query(sql, [questionId, userId]);

  return answers['result'] > 0;
};

const userChangesAnswer = async (questionId, questionAnswerId, userId) => {
  const sql = `
    UPDATE user_question_answer
    SET question_answers_id =?
    WHERE question_id = ?
    AND user_id = ?
    `;
  const { insertId: id } = await pool.query(sql, [questionAnswerId, questionId, userId]);

  return { id, questionId, questionAnswerId, userId };
};

const getUserQuizSumScore = async (userId, quizId) => {
  const sql = `
        SELECT COALESCE(SUM(q.score_individual_question), 0) score
        FROM questions as q
        WHERE q.question_id IN
            (SELECT u.question_id
                FROM user_question_answer as u
                JOIN question_answers as a ON u.question_answers_id = a.question_answers_id
                WHERE a.is_right = 1
                AND u.user_id = ?)
        AND q.quiz_id = ?;
    `;
  const [result] = await pool.query(sql, [userId, quizId]);

  return result;
};

const updateQuizStatus = async quizId => {
  const sql = `
  UPDATE
       quizzes
       SET is_active = 1
       WHERE
       quiz_id = ?`;

  return await pool.query(sql, [quizId]);
};

const updateQuizHistory = async (quizId, userId, startedAt, finishedAt) => {
  const sql = `
    INSERT INTO quizzes_history
    (quiz_id, user_id, started_at, finished_at)
    VALUES (?,?,?,?)
    `;

  return await pool.query(sql, [quizId, userId, startedAt, finishedAt]);
};
const hasInHistory = async (quizId, userId) => {
  const sql = `
    SELECT COUNT(q.quiz_id) result
    from quizzes_history q
    WHERE q.quiz_id = ?
    AND q.user_id = ?`;

  const [answers] = await pool.query(sql, [quizId, userId]);

  return answers['result'] > 0;
};

const getQuizHistory = async (quizId, userId) => {
  const sql = `
   select quiz_history_id, quiz_id, user_id, started_at, finished_at
   from quizzes_history 
   where quiz_id = ?
   and user_id = ?
    `;

  return await pool.query(sql, [quizId, userId]);
};

const getLeaderBoard = async () => {
  const sql = `
    select h.user_id , u.first_name as firstname, COALESCE(SUM(q.score_individual_question), 0) score
    from quizzes_history h
    join quizzes qz on qz.quiz_id = h.quiz_id
    join questions q on h.quiz_id = q.quiz_id
    join users u on u.user_id = h.user_id
    WHERE q.question_id IN
          (SELECT u.question_id
           FROM user_question_answer as u
                    JOIN question_answers as a ON u.question_answers_id = a.question_answers_id
           WHERE a.is_right = 1)
    and u.is_teacher = 0
    group by h.user_id
    order by score desc
    `;

  return await pool.query(sql);
};

const getQuizById = async quizId => {
  const sql = `
   select quiz_id, quiz_name, required_time, category_id, creator_id from quizzes where quiz_id = ?
    `;

  return await pool.query(sql, [quizId]);
};

const getHistoryForLoggedUser = async userId => {
  const sql = `
    SELECT q.quiz_id, q.quiz_name, q.required_time, h.user_id,h.started_at,h.finished_at
    FROM quizzes as q
    LEFT JOIN quizzes_history as h ON q.quiz_id = h.quiz_id
    WHERE h.user_id = ${userId};
    `;

  return await pool.query(sql);
};

export default {
  searchByQuiz,
  getQuizByIdAndCategoryId,
  getAllQuizzesInCurrentCategory,
  createQuiz,
  getByInQuiz,
  createQuestion,
  availableAnswers,
  getQuestionAnswersByQuestionId,
  getQuestionByQuizId,
  getUserQuizSumScore,
  userAnswers,
  answerExists,
  userChangesAnswer,
  updateQuizHistory,
  hasInHistory,
  getHistoryForLoggedUser,
  getQuizById,
  getQuizHistory,
  getLeaderBoard,
  updateQuizStatus
};
