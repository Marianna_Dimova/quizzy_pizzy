-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`categories` (
  `category_id` INT(11) NOT NULL AUTO_INCREMENT,
  `category_name` VARCHAR(400) NOT NULL,
  `avatar` VARCHAR(400) NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE INDEX `category_name_UNIQUE` (`category_name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 43
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(400) NOT NULL,
  `avatar` VARCHAR(600) NULL DEFAULT '0',
  `is_teacher` TINYINT(4) NOT NULL DEFAULT 0,
  `is_solving_quiz` VARCHAR(45) NULL DEFAULT '0',
  `gender` VARCHAR(1) NOT NULL DEFAULT 'f',
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`quizzes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`quizzes` (
  `quiz_id` INT(11) NOT NULL AUTO_INCREMENT,
  `quiz_name` VARCHAR(400) NOT NULL,
  `required_time` INT(10) NOT NULL,
  `avatar` VARCHAR(400) NULL DEFAULT '0',
  `category_id` INT(11) NOT NULL,
  `creator_id` INT(11) NOT NULL,
  `is_active` TINYINT NULL DEFAULT 0,
  PRIMARY KEY (`quiz_id`),
  UNIQUE INDEX `quiz_name_UNIQUE` (`quiz_name` ASC) VISIBLE,
  INDEX `fk_quizzes_categories1_idx` (`category_id` ASC) VISIBLE,
  INDEX `fk_quizzes_users1_idx` (`creator_id` ASC) VISIBLE,
  CONSTRAINT `fk_quizzes_categories1`
    FOREIGN KEY (`category_id`)
    REFERENCES `mydb`.`categories` (`category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_quizzes_users1`
    FOREIGN KEY (`creator_id`)
    REFERENCES `mydb`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 25
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`questions` (
  `question_id` INT(11) NOT NULL AUTO_INCREMENT,
  `question_text` VARCHAR(400) NOT NULL,
  `score_individual_question` INT(10) NOT NULL,
  `quiz_id` INT(11) NOT NULL,
  PRIMARY KEY (`question_id`),
  INDEX `fk_questions_quizzes1_idx` (`quiz_id` ASC) VISIBLE,
  CONSTRAINT `fk_questions_quizzes1`
    FOREIGN KEY (`quiz_id`)
    REFERENCES `mydb`.`quizzes` (`quiz_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 42
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`question_answers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`question_answers` (
  `question_answers_id` INT(11) NOT NULL AUTO_INCREMENT,
  `answer` VARCHAR(400) NOT NULL,
  `is_right` TINYINT(1) NULL DEFAULT NULL,
  `question_id` INT(11) NOT NULL,
  PRIMARY KEY (`question_answers_id`),
  INDEX `fk_question_choices_questions1_idx` (`question_id` ASC) VISIBLE,
  CONSTRAINT `fk_question_choices_questions1`
    FOREIGN KEY (`question_id`)
    REFERENCES `mydb`.`questions` (`question_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 56
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`quizzes_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`quizzes_history` (
  `quiz_history_id` INT(11) NOT NULL AUTO_INCREMENT,
  `quiz_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `started_at` VARCHAR(25) NULL DEFAULT NULL,
  `finished_at` VARCHAR(25) NULL DEFAULT NULL,
  PRIMARY KEY (`quiz_history_id`),
  INDEX `fk_quizzes_has_users_users1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_quizzes_has_users_quizzes1_idx` (`quiz_id` ASC) VISIBLE,
  CONSTRAINT `fk_quizzes_has_users_quizzes1`
    FOREIGN KEY (`quiz_id`)
    REFERENCES `mydb`.`quizzes` (`quiz_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_quizzes_has_users_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 81
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`user_question_answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`user_question_answer` (
  `user_question_answer_id` INT(11) NOT NULL AUTO_INCREMENT,
  `question_id` INT(11) NOT NULL,
  `question_answers_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`user_question_answer_id`),
  INDEX `fk_user_question_answer_questions1_idx` (`question_id` ASC) VISIBLE,
  INDEX `fk_user_question_answer_question_answers1_idx` (`question_answers_id` ASC) VISIBLE,
  INDEX `fk_user_question_answer_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_question_answer_question_answers1`
    FOREIGN KEY (`question_answers_id`)
    REFERENCES `mydb`.`question_answers` (`question_answers_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_question_answer_questions1`
    FOREIGN KEY (`question_id`)
    REFERENCES `mydb`.`questions` (`question_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_question_answer_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 94
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
