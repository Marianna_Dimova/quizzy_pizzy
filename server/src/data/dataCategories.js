import pool from './pool.js';

const getAllCategories = async () => {
  const sql = `
    SELECT category_id, category_name
    FROM categories
    `;
  return await pool.query(sql);
};

const searchByCategory = async (column, value) => {
  const sql = `
        SELECT category_id, category_name
        FROM categories
        WHERE ${column} LIKE '%${value}%' 
    `;

  return await pool.query(sql);
};

const getBy = async (column, value) => {
  const sql = `
        SELECT category_id, category_name
        FROM categories
        WHERE ${column} = ?
    `;
  const [category] = await pool.query(sql, [value]);

  return category;
};

const createCategory = async categoryName => {
  const sql = `
    INSERT INTO 
    categories(category_name)
    VALUES(?)
    `;

  const { insertId: id } = await pool.query(sql, [categoryName]);

  return { id, categoryName };
};

export default {
  getAllCategories,
  searchByCategory,
  getBy,
  createCategory
};
