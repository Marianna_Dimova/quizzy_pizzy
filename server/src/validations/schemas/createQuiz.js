export const createQuizSchema = {
  quizName: value => {
    if (!value) {
      return 'Quiz name is required';
    }

    if (typeof value !== 'string' || value.length < 2 || value.length > 50) {
      return 'Quiz name should be a string in range [3..50]';
    }

    return null;
  },

  requiredTime: value => {
    return value ? null : 'Quiz time is required';
  },
};
