export const createCategorySchema = {
  categoryName: value => {
    if (!value) {
      return 'Category is required';
    }

    if (typeof value !== 'string' || value.length < 2 || value.length > 50) {
      return 'Category should be a string in range [3..50]';
    }

    return null;
  },
};
