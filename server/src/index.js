import express from 'express';
import bodyParser from 'body-parser';
import passport from 'passport';
import cors from 'cors';
import { PORT } from './config.js';
import jwtStrategy from './auth/strategy.js';
import authController from './controllers/controllerAuth.js';
import usersController from './controllers/controllerUsers.js';
import questionController from './controllers/questionController.js';
import categoriesController from './controllers/controllerCategories.js';

const app = express();

passport.use(jwtStrategy);
app.use(cors(), bodyParser.json());
app.use(passport.initialize());
app.use('/auth', authController);
app.use('/users', usersController);
app.use('/questions', questionController);
app.use('/categories', categoriesController);
app.use('/public', express.static('images'));

app.use((req, res, next) => {
  res.status(400).send({ message: 'An unexpected error occurred, our developers are working hard to resolve it.' });
});

app.all('*', (_, res) => res.status(404).send({ message: 'Resource not found!' }));

app.listen(PORT, () => console.log(`App is listening on port ${PORT}`));
