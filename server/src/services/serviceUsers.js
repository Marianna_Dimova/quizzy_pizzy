import bcrypt from 'bcrypt';
import usersData from '../data/dataUsers.js';
import { DEFAULT_USER_ROLE } from '../config.js';
import { ERRORS } from '../constants.js';

const createUser = async ({ username, password, firstName, lastName, gender }) => {
  const existingUser = await usersData.getBy('username', username);

  if (existingUser) {
    return { error: ERRORS.DUPLICATE_RECORD, user: null };
  }

  const passwordHash = await bcrypt.hash(password, 10);
  const user = await usersData.create(username, passwordHash, firstName, lastName, DEFAULT_USER_ROLE, gender);

  return { error: null, user: user };
};

const getAllUsers = async filter => {
  return filter ? await usersData.searchBy('username', filter) : await usersData.getAll();
};

const getUserById = async userId => {
  const user = await usersData.getBy('user_id', userId);

  return user ? { error: null, user } : { error: ERRORS.RECORD_NOT_FOUND, user: null };
};

const signInUser = async (username, password) => {
  const user = await usersData.getWithRole(username);

  return !user || !(await bcrypt.compare(password, user.password))
    ? { error: ERRORS.INVALID_CREDENTIALS, user: null }
    : { error: null, user };
};

export default {
  createUser,
  getAllUsers,
  getUserById,
  signInUser
};
