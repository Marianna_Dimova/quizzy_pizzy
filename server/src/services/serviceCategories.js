import { ERRORS } from '../constants.js';
import categoriesData from '../data/dataCategories.js';

const getAllCategories = async filter => {
  const categories = filter
    ? await categoriesData.searchByCategory('category_name', filter)
    : await categoriesData.getAllCategories();

  return categories.map(({ category_id, category_name }) => ({ categoryId: category_id, categoryName: category_name }));
};

const getOneCategoryId = async categoryId => {
  const category = await categoriesData.getBy('category_id', categoryId);

  return category ? { error: null, category: category } : { error: ERRORS.RECORD_NOT_FOUND, category: null };
};

const createOneCategory = async ({ name }) => {
  const existingCategory = await categoriesData.getBy('category_name', name);

  if (existingCategory) {
    return { error: ERRORS.DUPLICATE_RECORD, category: null };
  }

  const category = await categoriesData.createCategory(name);

  return { error: null, category: category };
};

export default {
  getAllCategories,
  createOneCategory,
  getOneCategoryId
};
