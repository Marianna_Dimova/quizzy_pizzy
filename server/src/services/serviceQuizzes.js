import { ERRORS } from '../constants.js';
import quizzesData from '../data/dataQuizzes.js';
import categoryData from '../data/dataCategories.js';

const getAllQuizzes = async (id, userId) => {
  const quizzes = await quizzesData.getAllQuizzesInCurrentCategory(id, userId);
  const mappedQuizzes = quizzes.map(({ quiz_id, quiz_name, required_time, category_id, creator_id, is_active }) => ({
    quizId: quiz_id,
    quizName: quiz_name,
    requiredTime: required_time,
    categoryId: category_id,
    creatorId: creator_id,
    isActive: is_active
  }));
  return mappedQuizzes.length === 0
    ? { error: ERRORS.RECORD_NOT_FOUND, quiz: null }
    : { error: null, quizzes: mappedQuizzes };
};

const createOneQuiz = async ({ quizName, requiredTime, creatorId }, categoryId) => {
  const category = await categoryData.getBy('category_id', +categoryId);
  const existingQuiz = await quizzesData.getByInQuiz('quiz_name', quizName);

  if (existingQuiz) {
    return { error: ERRORS.DUPLICATE_RECORD, quiz: null };
  }

  if (!category) {
    return { error: ERRORS.RECORD_NOT_FOUND, category: null };
  }

  const quiz = await quizzesData.createQuiz(quizName, requiredTime, categoryId, creatorId);

  return { error: null, quiz: quiz };
};

const setActiveStatusQuiz = async quizId => {
  const quiz = await quizzesData.getQuizById(+quizId);

  if (!quiz) {
    return { error: ERRORS.RECORD_NOT_FOUND, updatedQuiz: null };
  }
  const updatedQuiz = await quizzesData.updateQuizStatus(quizId);
  return { error: null, updatedQuiz: updatedQuiz };
};

const createQuestions = async ({ question_text, score_individual_question, quizId }) => {
  const question = await quizzesData.createQuestion(question_text, score_individual_question, quizId);

  return { error: question ? 'success' : ERRORS.RECORD_NOT_FOUND, question: question };
};

const createAnswers = async answerCreate => {
  const { answer, is_right, questionId } = answerCreate;
  const answers = await quizzesData.availableAnswers(answer, is_right, questionId);

  return { error: answers ? 'success' : ERRORS.RECORD_NOT_FOUND, answers: answers };
};

const setUserAnswers = async (userId, addAnswers) => {
  for (const answer of addAnswers) {
    await quizzesData.userAnswers(answer.questionId, answer.questionAnswersId, userId);
  }
};

const updatedAnswer = async (questionId, questionAnswersId, userId) => {
  const answerExists = await quizzesData.answerExists(questionId, userId);
  const addAnswer = await quizzesData.userAnswers(questionId, questionAnswersId, userId);
  const updateAnswer = await quizzesData.userChangesAnswer(questionId, questionAnswersId, userId);

  return answerExists ? updateAnswer : addAnswer;
};

const getAllQuestionsForQuiz = async quizId => {
  let questions = [];
  for (let question of await quizzesData.getQuestionByQuizId(quizId)) {
    let answers = await quizzesData.getQuestionAnswersByQuestionId(question.question_id);
    questions.push({ ...question, answers: answers });
  }

  return {
    error: questions ? 'success' : ERRORS.RECORD_NOT_FOUND,
    questions: questions
  };
};

const getOneQuiz = async (quizId, categoryId) => {
  const quiz = await quizzesData.getQuizByIdAndCategoryId(quizId, categoryId);

  return {
    error: quiz ? 'success' : ERRORS.RECORD_NOT_FOUND,
    message: quiz ? null : 'Quiz ' + quizId + ' not found!',
    quiz: quiz
  };
};

const getQuiz = async quizId => {
  const quizzes = await quizzesData.getQuizById(quizId);
  const mappedQuizzes = quizzes.map(({ quiz_id, quiz_name, required_time, category_id, creator_id }) => ({
    quizId: quiz_id,
    quizName: quiz_name,
    requiredTime: required_time,
    categoryId: category_id,
    creatorId: creator_id
  }));

  return mappedQuizzes[0];
};

const hasInQuizzesHistory = async (quizId, userId) => {
  return await quizzesData.hasInHistory(quizId, userId);
};

const getQuizzesHistory = async (quizId, userId) => {
  return (await quizzesData.getQuizHistory(quizId, userId))[0];
};

const getLeaderBoard = async (quizId, userId) => {
  let scoresResult = await quizzesData.getLeaderBoard(quizId, userId);
  let studentScores = [];
  for (let i = 0; i < scoresResult.length; i++) {
    studentScores.push(scoresResult[i]);
  }

  return studentScores;
};

const addQuizHistory = async (quizId, userId, startedAt, finishedAt) => {
  const added = await quizzesData.updateQuizHistory(quizId, userId, startedAt, finishedAt);

  return added ? { error: null, history: added } : { error: ERRORS.RECORD_NOT_FOUND, history: null };
};

const calculateUserQuizScore = async (userId, quizId) => {
  const sum = await quizzesData.getUserQuizSumScore(userId, quizId);

  return sum
    ? { status: 'success', quiz: quizId, scores: sum ? sum.score : 0 }
    : { status: 'error', message: 'Something went wrong. Please ask your sys admin' };
};

const displayHistoryForLoggedUser = async userId => {
  const history = await quizzesData.getHistoryForLoggedUser(userId);

  return history ? { error: null, history: history } : { error: ERRORS.RECORD_NOT_FOUND, history: null };
};

export default {
  getAllQuizzes,
  createOneQuiz,
  getOneQuiz,
  createQuestions,
  createAnswers,
  getAllQuestionsForQuiz,
  setUserAnswers,
  updatedAnswer,
  hasInQuizzesHistory,
  addQuizHistory,
  displayHistoryForLoggedUser,
  calculateUserQuizScore,
  getQuiz,
  getQuizzesHistory,
  getLeaderBoard,
  setActiveStatusQuiz
};
