import passportJwt from 'passport-jwt';
import { PRIVATE_KEY } from './../config.js';

const options = { secretOrKey: PRIVATE_KEY, jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken() };

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
  const userData = {
    userId: payload.userId,
    username: payload.username,
    firstName: payload.firstName,
    lastName: payload.lastName,
    isTeacher: payload.isTeacher,
    isSolvingQuiz: payload.isSolvingQuiz
  };

  done(null, userData);
});

export default jwtStrategy;
