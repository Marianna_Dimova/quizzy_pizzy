import jwt from 'jsonwebtoken';
import { PRIVATE_KEY, TOKEN_LIFETIME } from './../config.js';

const createToken = payload => {
  return jwt.sign(payload, PRIVATE_KEY, { expiresIn: TOKEN_LIFETIME });
};

export default createToken;
