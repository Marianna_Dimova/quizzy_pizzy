import express from 'express';
import usersService from '../services/serviceUsers.js';
import { authMiddleware } from '../auth/authMiddleware.js';
import { createValidator, createUserSchema } from '../validations/index.js';
import { ERRORS } from '../constants.js';

const usersController = express.Router();

usersController
  .post('/', createValidator(createUserSchema), async ({ body: createData }, res) => {
    const { error, user } = await usersService.createUser(createData);

    error === ERRORS.DUPLICATE_RECORD
      ? res.status(409).send({ message: 'Name not available' })
      : res.status(201).send(user);
  })
  .get('/', async ({ query: { search } }, res) => {
    const users = await usersService.getAllUsers(search);

    res.status(200).send(users);
  })
  .get('/:id', authMiddleware, async ({ params: { id } }, res) => {
    const { error, user } = await usersService.getUserById(+id);

    error === ERRORS.RECORD_NOT_FOUND
      ? res.status(404).send({ message: 'User not found!' })
      : res.status(200).send(user);
  });

export default usersController;
