import express from 'express';
import { authMiddleware, roleMiddleware } from '../auth/authMiddleware.js';
import { ERRORS } from '../constants.js';
import quizzesService from '../services/serviceQuizzes.js';
import serviceUsers from '../services/serviceUsers.js';

const questionController = express.Router();

questionController
  .post('/:id/create/', authMiddleware, roleMiddleware(1), async ({ body: newQuestion, params: { id } }, res) => {
    const { error, question } = await quizzesService.createQuestions({
      question_text: newQuestion.question_text,
      score_individual_question: newQuestion.score_individual_question,
      quizId: id
    });

    if (error !== ERRORS.DUPLICATE_RECORD) {
      for (let currentAnswer of newQuestion.answers) {
        await quizzesService.createAnswers({
          answer: currentAnswer.answer,
          is_right: currentAnswer.is_right,
          questionId: question.id
        });
      }
      res.status(201).send(newQuestion);
    } else {
      res.status(409).send({ message: 'Question name not available' });
    }
  })
  .get('/start/quiz/:quizId', authMiddleware, async ({ params: { quizId }, user: { userId, isTeacher } }, res) => {
    const isInHistory = await quizzesService.hasInQuizzesHistory(quizId, userId);
    const { questions } = await quizzesService.getAllQuestionsForQuiz(quizId);
    const { requiredTime } = await quizzesService.getQuiz(quizId);

    let startedAt = new Date();
    let finishedAt = new Date();
    const finish = new Date(finishedAt.setMinutes(finishedAt.getMinutes() + requiredTime));

    let response = { quizId, startedAt, finish, requiredTime, questions };

    if (isTeacher) {
      res.status(201).send(response);
      await quizzesService.addQuizHistory(quizId, userId, startedAt, finish);
    } else {
      if (isInHistory) {
        res.status(201).send({
          status: 'error',
          message: 'Already finished this quiz'
        });
      } else {
        res.status(201).send(response);
        await quizzesService.addQuizHistory(quizId, userId, startedAt, finish);
      }
    }
  })
  .post('/quiz/:quizId/submit', authMiddleware, async (req, res) => {
    const userId = req.user.userId;
    const { quizId } = req.params;
    const { questions } = await quizzesService.getAllQuestionsForQuiz(quizId);
    const { answers } = req.body;

    if (answers && questions.length === answers.length) {
      await quizzesService.setUserAnswers(userId, answers);
      const scoresResponse = await quizzesService.calculateUserQuizScore(userId, quizId);

      return res.status(200).send(scoresResponse);
    } else {
      res
        .status(400)
        .send({ status: 'error', message: 'Something went wrong with your answers. Please submit all answers' });
    }
  })
  .get('/leader_board', authMiddleware, async (_, res) => {
    const users = (await serviceUsers.getAllUsers()).filter(u => u.is_teacher === 0);
    const leaderBoard = [];

    for (const user of users) {
      const { history } = await quizzesService.displayHistoryForLoggedUser(user.user_id);

      let score = 0;
      for (const solvedQuiz of history) {
        const solution = await quizzesService.calculateUserQuizScore(user.user_id, solvedQuiz.quiz_id);
        score += solution.scores;
      }

      leaderBoard.push({ ...user, score });
    }

    res.status(200).send({
      status: 'success',
      leaderBoard: leaderBoard
        .sort((a, b) => b.score - a.score)
        .map(({ user_id: userId, first_name: firstName, last_name: lastName, gender, score, username }) => ({
          userId,
          username,
          firstName,
          lastName,
          gender,
          score
        }))
    });
  })
  .get('/createQuestions/:quizId', authMiddleware, async ({ params: { quizId } }, res) => {
    const { questions } = await quizzesService.getAllQuestionsForQuiz(quizId);
    let response = { quizId, questions };
    res.status(200).send(response);
  });

export default questionController;
