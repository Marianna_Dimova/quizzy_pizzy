import express from 'express';
import categoriesService from './../services/serviceCategories.js';
import { authMiddleware, roleMiddleware } from '../auth/authMiddleware.js';
import quizzesService from './../services/serviceQuizzes.js';
import { ERRORS } from '../constants.js';
const categoriesController = express.Router();

categoriesController
  .get('/', authMiddleware, async ({ query: { search } }, res) => {
    const categories = await categoriesService.getAllCategories(search);
    res.status(200).send(categories);
  })
  .get('/:id', authMiddleware, async ({ params: { id } }, res) => {
    const category = await categoriesService.getOneCategoryId(+id);
    category ? res.status(200).send(category) : res.status(404).send({ message: 'The category is not found!' });
  })
  .post('/', authMiddleware, roleMiddleware(1), async ({ body: { name } }, res) => {
    const { error, category } = await categoriesService.createOneCategory({ name });

    error === ERRORS.DUPLICATE_RECORD
      ? res.status(409).send({ message: 'Category name not available' })
      : res.status(201).send(category);
  })
  .get('/:id/quizzes', authMiddleware, async ({ params: { id }, user: { userId } }, res) => {
    const { error, quizzes } = await quizzesService.getAllQuizzes(+id, +userId);

    error === ERRORS.RECORD_NOT_FOUND
      ? res.status(404).send({ message: 'Quizzes not found' })
      : res.status(200).send(quizzes);
  })
  .post(
    '/:id/quizzes',
    authMiddleware,
    roleMiddleware(1),
    async ({ body, params: { id: categoryId }, user: { userId: creatorId } }, res) => {
      const createDataQuiz = { creatorId, ...body };
      if (!createDataQuiz) {
        return res.status(400).send({ message: 'The quiz should has valid content!' });
      }

      const { error, quiz } = await quizzesService.createOneQuiz(createDataQuiz, +categoryId);

      error === ERRORS.DUPLICATE_RECORD
        ? res.status(409).send({ message: 'Quiz name not available' })
        : res.status(201).send(quiz);
    }
  )
  .get('/:categoryId/quizzes/:quizId', authMiddleware, async ({ params: { categoryId, quizId } }, res) => {
    const quiz = await quizzesService.getOneQuiz(+quizId, +categoryId);
    quiz.error === 'success' ? res.status(200).send(quiz) : res.status(404).send(quiz);
  })
  .put(
    '/:categoryId/quizzes/:quizId/status',
    authMiddleware,
    roleMiddleware(1),
    async ({ params: { quizId } }, res) => {
      const { updatedQuiz, error } = quizzesService.setActiveStatusQuiz(+quizId);

      error === ERRORS.RECORD_NOT_FOUND
        ? res.status(409).send({ message: 'Quiz not found' })
        : res.status(201).send(updatedQuiz);
    }
  )
  .post(
    '/:categoryId/quizzes/:quizId/answer',
    authMiddleware,
    roleMiddleware(1),
    async ({ body: createAnswer, params: { quizId } }, res) => {
      if (!createAnswer) {
        return res.status(400).send({ message: 'The answer should has valid content!' });
      }

      const { error, answers } = await quizzesService.createAnswers(createAnswer, +quizId);

      error === ERRORS.DUPLICATE_RECORD
        ? res.status(409).send({ message: 'Answer name not available' })
        : res.status(201).send(answers);
    }
  )
  .post(
    '/quizzes/:quizId/history',
    authMiddleware,
    async ({ body: { startedAt, finishedAt }, params: { quizId }, user: { userId, isTeacher: role } }, res) => {
      const isInHistory = quizzesService.hasInQuizzesHistory(quizId, userId);
      const historyCreate = quizzesService.addQuizHistory(quizId, userId, startedAt, finishedAt);

      if (!isInHistory) {
        res.status(200).send({ message: 'Attempt to resolve quiz = 0', start: historyCreate });
      } else if (isInHistory && role === 1) {
        res.status(200).send({ message: 'Teachers are able to resolve quizzes multiple times', start: historyCreate });
      } else if (isInHistory && role === 0) {
        res.status(400).send({ message: 'Students are able to resolve quizzes only one time' });
      }
    }
  )
  .get('/quizzes/history', authMiddleware, async ({ user: { userId } }, res) => {
    const { error, history } = await quizzesService.displayHistoryForLoggedUser(+userId);

    error === ERRORS.RECORD_NOT_FOUND
      ? res.status(404).send({ message: 'Quizzes not found' })
      : res.status(200).send(history);
  });

export default categoriesController;
