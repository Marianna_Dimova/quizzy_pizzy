import express from 'express';
import createToken from './../auth/createToken.js';
import usersService from './../services/serviceUsers.js';
import { ERRORS } from '../constants.js';

const authController = express.Router();

authController
  .post('/login', async (req, res) => {
    const { username, password } = req.body;
    const { error, user } = await usersService.signInUser(username, password);

    if (error === ERRORS.INVALID_CREDENTIALS) {
      res.status(400).send({ message: 'Invalid username/password' });
    } else {
      const payload = {
        userId: user.user_id,
        username: user.username,
        firstName: user.first_name,
        lastName: user.last_name,
        isTeacher: user.is_teacher,
        gender: user.gender
      };

      const token = createToken(payload);

      res.status(200).send({ token });
    }
  })
  .delete('/logout', (req, res) => {
    res.status(200).json({ message: 'Successful logout!' });
  });

export default authController;
